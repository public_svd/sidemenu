//
//  SlideMenuActions.swift
//

import Foundation

enum WebPoints {
    case Feeds, TimeLine, GoodNews, Rewards, Terms, Support, Privacy, Essay, CommunityContest, Time4VR, Sweepstakes
    
    var urlPath: String {
        get {
            var path = Defines.baseURLString
            
            switch self {
            case .Feeds:
                path += "/members/home"
            case .TimeLine:
                path += "/profile/" + "\(Tools.sharedUser.userId!)"
            case .GoodNews:
                path += "/goodnews"
            case .Rewards:
                path += "/webshop"
            case .Terms:
                path += "/help/terms"
            case .Support:
                path += "/webshop/customer-service"
            case .Privacy:
                path += "/pages/privacy-policy"
            case .Essay:
                path += "/pages/u4g-essay-and-photo-contest"
            case .CommunityContest:
                path += "/pages/u4g-community-contest"
            case .Time4VR:
                path += "/pages/time4vr-giveaway"
            case .Sweepstakes:
                path += "/pages/u4g-december-sweepstakes-official-rules"
            }
            
            return path
        }
    }
    
    var safariURL: String {
        get {
            var result = urlPath
            if let sessId = Tools.sharedUser.sessionId {
                result += "?\(sessId)"
            }
            return result
        }
    }
    
    var request: NSURLRequest {
        get {
            let request = NSMutableURLRequest(URL: NSURL(string: urlPath)!)
            var cookiesArray: [String] = []
            if let sessID = Tools.sharedUser.sessionId {
                cookiesArray.append(sessID)
            }
            if let front = Tools.sharedUser.frontendID {
                cookiesArray.append(front)
            }
            
            if cookiesArray.count > 0 {
                let cookies = cookiesArray.joinWithSeparator("; ")
                request.setValue(cookies, forHTTPHeaderField: "Cookie")
            }
            request.timeoutInterval = Defines.requestTimeOut
            
            return request.copy() as! NSURLRequest
        }
    }
    
    var title: String {
        get {
            var title = ""
            switch self {
            case .Feeds:
                title = "Feeds"
            case .TimeLine:
                title = "Timeline"
            case .GoodNews:
                title = "Good News"
            case .Rewards:
                title = "Rewards"
            case .Terms:
                title = "Terms & Conditions"
            case .Support:
                title = "Support"
            case .Privacy:
                title = "Privacy Policy"
            case .Essay:
                title = "Essay & Photo Contest"
            case .CommunityContest:
                title = "U4G Community Contest"
            case .Time4VR:
                title = "Time4VR giveaway"
            case .Sweepstakes:
                title = "December Sweepstakes"
            }
            return title
        }
    }
}