//
//  SideContainerVC.swift
//  SidePanelTest
//
//  Created by SvD on 05.08.15.
//  Copyright (c) 2015 SvD. All rights reserved.
//

import UIKit

enum SlideOutState {
    case BothCollapsed
    case LeftPanelExpanded
    case RightPanelExpanded
}

protocol EventsNeeder {
    func eventsDidUpdated(events:[EventModel])
}

//MARK: - Side Panel
protocol SidePanelDelegate {
    func actionWith(action action: Actions, closeAllPanels: Bool)
}

protocol SidePanelProtocol {
    func setSidePanelDelegate(delegate: SidePanelDelegate)
}

//MARK: - CenterController
protocol CenterViewControllerDelegate {
    func toggleLeftPanel()
    func toggleRightPanel()
    func collapseSidePanels()
    func setCenterVCForAction(action: Actions)
}

protocol CenterControllerProtocol {
    func setCenterDelegate(delegate: CenterViewControllerDelegate)
}


//MARK: - SideContainerVC
class SideContainerVC: UIViewController {

    @IBOutlet weak var leftSidePanel: UIView!
    @IBOutlet weak var centralPanel: UIView!
    @IBOutlet weak var viewShadow: UIView!

    var centerNavigationController: UINavigationController!
    
    var centerViewController: CenterControllerProtocol?
    var leftViewController: SidePanelProtocol?
    var rightViewController: SidePanelProtocol?

    var statusBarView: UIView {
        let result = UIView(frame: CGRectMake(0, -20, CGRectGetWidth(UIScreen.mainScreen().bounds), 20))
        result.backgroundColor = Colors.statusBarColor
        return result
    }

    var currentState: SlideOutState = .BothCollapsed {
        didSet {
            animateToCurrentState()
        }
    }

    var panGestureRecognizer:  UIScreenEdgePanGestureRecognizer?  //UIPanGestureRecognizer?
    
    override func addChildViewController(childController: UIViewController) {
        super.addChildViewController(childController)
        var controller: UIViewController = childController
        if let child = childController as? UINavigationController {
            centerNavigationController = child
            statusBarView.removeFromSuperview()
            centerNavigationController.navigationBar.addSubview(statusBarView)
            controller = child.topViewController!
        }
        if let ctrl = controller as? CenterControllerProtocol {
            ctrl.setCenterDelegate(self)
            centerViewController = ctrl
            if let panelDelegate = centerViewController as? SidePanelDelegate {
                leftViewController?.setSidePanelDelegate(panelDelegate)
            }
        }
        else if let ctrl = controller as? SidePanelProtocol {
            if let panelDelegate = centerViewController as? SidePanelDelegate {
                ctrl.setSidePanelDelegate(panelDelegate)
            }
            leftViewController = ctrl
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

//        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: "handlePanGesture:")
//        view.addGestureRecognizer(panGestureRecognizer!)

        panGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(SideContainerVC.handlePanGesture(_:)))
        panGestureRecognizer?.edges = UIRectEdge.Left
        view.addGestureRecognizer(panGestureRecognizer!)

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SideContainerVC.handleTapGesture(_:)))
        viewShadow.addGestureRecognizer(tapGestureRecognizer)

        let panShadowRecognizer = UIPanGestureRecognizer(target: self, action: #selector(SideContainerVC.handlePanGesture(_:)))
        viewShadow.addGestureRecognizer(panShadowRecognizer)

        if let leftVC = leftViewController as? UIViewController {
            let panMenuRecognizer = UIPanGestureRecognizer(target: self, action: #selector(SideContainerVC.handlePanGesture(_:)))
            leftVC.view.addGestureRecognizer(panMenuRecognizer)
        }

        setupConstraints()
    }

    func `catch`(notif: NSNotification) {
        #if DEBUG
            print("Did catch notification: \(notif.name)")
        #endif
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        leftSidePanel.center.x = -CGRectGetMidX(leftSidePanel.frame)
        leftSidePanel.hidden = true
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        Tools.configNavBarForMain()
        centerViewController?.setCenterDelegate(self)

        if let panelDelegate = centerViewController as? SidePanelDelegate {
            leftViewController?.setSidePanelDelegate(panelDelegate)
        }
        
    }

    func setupConstraints() { }
}

//MARK: CenterViewControllerDelegate
extension SideContainerVC: CenterViewControllerDelegate {

    func setCenterVCForAction(action: Actions) {
        switch action {
        case .Legal, .Support, .About:
            dispatch_async(dispatch_get_main_queue()) {[weak self] () -> Void in
                for vc in (self?.childViewControllers)! {
                    if let nc = vc as? UINavigationController {
                        let newCentrVC = UIStoryboard.mainStoryboard().instantiateViewControllerWithIdentifier(action.controllerID)
                        if let webPoint = action.webPoint, let terms = newCentrVC as? TermsVC {
                            terms.webPoint = webPoint
                        }
                        nc.pushViewController(newCentrVC, animated: true)
                    }
                }
            }

        case .Profile:
            U4G_Core.userProfile(Tools.sharedUser) { (dict, error) -> () in

                if let response = dict where error == nil, let meta = response["userMetadata"] as? sdJSONDict {
                    let profile = Tools.sharedUserProfile
                    profile.jsonValue = meta

                    dispatch_async(dispatch_get_main_queue()) {[weak self] () -> Void in
                        for vc in (self?.childViewControllers)! {
                            if let nc = vc as? UINavigationController {
                                let profileVC = UIStoryboard.mainStoryboard().instantiateViewControllerWithIdentifier(action.controllerID) as! SDProfileVC
                                let coordinator = ProfileCoordinator()
                                coordinator.userProfile = Tools.sharedUserProfile
                                profileVC.coordinator = coordinator
                                nc.pushViewController(profileVC, animated: true)
                            }
                        }
                    }
                }
            }
        default:
            break
        }
    }
    
    private func updateCenterToID(id: String) {
        let vc = UIStoryboard.mainStoryboard().instantiateViewControllerWithIdentifier(id) 
        updateCenterVC(vc)
    }

    private func updateCenterVC(viewController: UIViewController) {
        if let centerVC = viewController as? CenterControllerProtocol {
            centerViewController = centerVC
            centerVC.setCenterDelegate(self)
        }
        centerNavigationController?.viewControllers = [viewController]
    }


    func toggleLeftPanel() {
        if currentState == .BothCollapsed {
            currentState = .LeftPanelExpanded
        }
        else {
            currentState = .BothCollapsed
        }
    }

    func toggleRightPanel() {

    }

    func collapseSidePanels() {
        currentState = .BothCollapsed
    }

    func animateToCurrentState() {
        if let vc = centerViewController as? UIViewController {
            vc.view.endEditing(true)
        }
        switch currentState {
        case .LeftPanelExpanded:
            if let menu = leftViewController as? MenuVC {
                menu.updateUserInfo()
            }
            showShadowForSidePanel(true)
            leftSidePanel.hidden = false
            animateLeftPanelXPosition(targetPosition: 0.0)
            break
        case .BothCollapsed:
            let targetPositionX = -CGRectGetWidth(leftSidePanel.frame)
            animateLeftPanelXPosition(targetPosition: targetPositionX - 10.0) {[weak self] finished in
                self?.showShadowForSidePanel(false)
                self?.leftSidePanel.frame.origin.x = targetPositionX
                self?.leftSidePanel.hidden = true
            }
            break
        default:
            break
        }
    }

    func animateLeftPanelXPosition(targetPosition targetPosition: CGFloat, completion: ((Bool) -> Void)! = nil) {
        UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .CurveEaseInOut, animations: {
            self.leftSidePanel.frame.origin.x = targetPosition
            if Int(targetPosition) == 0 {
                self.viewShadow.alpha = 1.0
            }
            else {
                self.viewShadow.alpha = 0.0
            }
            }, completion: completion)
    }

    func showShadowForSidePanel(shouldShowShadow: Bool) {
        if (shouldShowShadow) {
            leftSidePanel.layer.shadowOpacity = 0.8
        } else {
            leftSidePanel.layer.shadowOpacity = 0.0
        }
    }

}

// MARK: Gesture recognizer
extension SideContainerVC: UIGestureRecognizerDelegate {

    func handlePanGesture(recognizer: UIPanGestureRecognizer) {

//        let gestureIsDraggingFromLeftToRight = (recognizer.velocityInView(view).x > 0)

        switch(recognizer.state) {
        case .Began:
            if (currentState == .BothCollapsed) {
                if let vc = centerViewController as? UIViewController {
                    vc.view.endEditing(true)
                }
                showShadowForSidePanel(true)
            }

        case .Changed:
            let recOffset = recognizer.translationInView(view).x
            let newOrigin = CGRectGetMinX(leftSidePanel.frame) + recOffset
            let panelWidth = CGRectGetWidth(leftSidePanel.frame)
            if newOrigin <= 0 && newOrigin >= -panelWidth {
                leftSidePanel.hidden = false
                leftSidePanel.frame.origin.x = newOrigin
                viewShadow.alpha = (newOrigin + panelWidth) / panelWidth
            }
            recognizer.setTranslation(CGPointZero, inView: view)

        case .Ended:
            if (leftViewController != nil) {
                let hasMovedGreaterThanHalfway = leftSidePanel.center.x > 0
                if hasMovedGreaterThanHalfway {
                    currentState = .LeftPanelExpanded
                }
                else {
                    currentState = .BothCollapsed
                }
            }

        default:
            break
        }
    }
    
    func handleTapGesture(recognizer: UITapGestureRecognizer) {
        collapseSidePanels()
    }
}
