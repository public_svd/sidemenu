//
//  CenterVC.swift
//  U4G
//
//  Created by SvD on 04.08.15.
//  Copyright (c) 2015 SvD. All rights reserved.
//

import UIKit

class CentralBaseVC: UIViewController {

    var centerDelegate: CenterViewControllerDelegate?

    lazy var labKScore: UILabel = {
        let result = UILabel(frame: CGRectMake(0, 3, 50, 20))
        result.textAlignment = NSTextAlignment.Right
        result.textColor = UIColor.whiteColor()
        result.font = UIFont.init(name: Fonts.medium, size: 12)
        return result
    }()

    // MARK: - Actions
    @IBAction func unwindSegue(sender: UIStoryboardSegue){
    }

    @IBAction func actBack(sender: AnyObject?) {
        navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func actToggleLeft(sender: AnyObject) {
        centerDelegate?.toggleLeftPanel()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
        updateKScore()
    }

    func updateKScore() {
        if let points = Tools.sharedUser.kPoints {
            labKScore.text = "\(points)"
        }
        else {
            labKScore.text = "0"
        }
    }

    func setupNavigationBar() {

        setupNavBar()
        setupLeftBarItem()
        setupRightBarItem()
    }

    func setupNavBar() {
        navigationController?.navigationBar.translucent = false
    }

    func setupLeftBarItem() {
        let barButtonMenu = UIBarButtonItem(image: UIImage(named: "icon_menu"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(CentralBaseVC.actToggleLeft(_:)))
        navigationItem.leftBarButtonItem = barButtonMenu
    }

    func setupRightBarItem() {
        let rightView = UIView(frame: CGRectMake(0, 0, 50, 40))
        let labKPoints = UILabel(frame: CGRectMake(0, 17, 50, 20))
        labKPoints.textAlignment = NSTextAlignment.Right
        labKPoints.textColor = UIColor.whiteColor()
        labKPoints.font = UIFont(name: Fonts.medium, size: 9)
        labKPoints.text = "K-POINTS"

        rightView.addSubview(labKScore)
        rightView.addSubview(labKPoints)

        let barItemScore = UIBarButtonItem(customView: rightView)
        navigationItem.rightBarButtonItem = barItemScore
    }

}

extension CentralBaseVC: CenterControllerProtocol {
    func setCenterDelegate(delegate: CenterViewControllerDelegate) {
        centerDelegate = delegate
    }
}

extension CentralBaseVC: SidePanelDelegate {

    func actionWith(action action: Actions, closeAllPanels: Bool) {

        switch action {
        case .Logout:
            U4G_Core.logout { (dict, error) -> () in
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    NSNotificationCenter.defaultCenter().postNotification(NSNotification(name: Defines.kLoginNeededNotification, object: nil))
                }
            }

        default:
            centerDelegate?.setCenterVCForAction(action)
        }

        if closeAllPanels {
            centerDelegate?.collapseSidePanels()
        }
    }


    func doMenuGoToWeb(urlString: String) {

        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.doMenuGoToWeb(urlString)
    }
}

