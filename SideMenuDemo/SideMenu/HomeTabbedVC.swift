//
//  HomeTabbedVC.swift
//

import UIKit
import AssetsLibrary


protocol TabInnerControllerProtocol {
    func setInnerDelegate(delegate: TabInnerControllerDelegate)
    func navigationBarItems() -> [UINavigationItem]?
    func customTitleView() -> UIView?
    func filterTitles() -> [String]
    func controllersID() -> [String]
}

protocol TabInnerControllerDelegate {
}


class HomeTabbedVC: UITabBarController {

    var centerDelegate: CenterViewControllerDelegate?
    @IBOutlet weak var btnPost: UIButton!
    @IBOutlet weak var btnShoot: UIButton!

    // MARK: - Actions
    @IBAction func unwindSegue(sender: UIStoryboardSegue) {}
    @IBAction func unwindToTab(sender: UIStoryboardSegue) {}

    @IBAction func actToggleLeft(sender: AnyObject) {
        centerDelegate?.toggleLeftPanel()
    }

    @IBAction func actPost() {
        #if DEBUG
            print("Post")
        #endif
    }

    @IBAction func actShoot() {
        #if DEBUG
            print("Shoot")
        #endif
    }

    func actAdd() {
        performSegueWithIdentifier("toCamera", sender: nil)
    }

    func addCenterButtonWithImage() {
        let buttonImage = UIImage(named: "plus_ic")!
        let buttonImagePressed = UIImage(named: "bl_plus_ic")!
        let btnSize: CGFloat = 49
        let button = UIButton(type: .Custom)
        button.autoresizingMask = [.FlexibleRightMargin, .FlexibleLeftMargin, .FlexibleBottomMargin, .FlexibleTopMargin]
        button.frame = CGRectMake(0.0, 0.0, btnSize, btnSize)
        button.setBackgroundImage(buttonImage, forState: .Normal)
        button.setBackgroundImage(buttonImagePressed, forState: .Highlighted)

        button.addTarget(self, action: #selector(HomeTabbedVC.actAdd), forControlEvents: UIControlEvents.TouchUpInside)

        let heightDifference: CGFloat = btnSize - tabBar.frame.size.height
        if heightDifference <= 0 {
            button.center = self.tabBar.center
        }
        else {
            var center = tabBar.center
            center.y = center.y - heightDifference / 2.0
            button.center = center
        }

        view.addSubview(button)
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.whiteColor()

        navigationController?.navigationBar.translucent = false
        let barButtonMenu = UIBarButtonItem(image: UIImage(named: "contact_icon"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(HomeTabbedVC.actToggleLeft(_:)))
        navigationItem.leftBarButtonItem = barButtonMenu
        delegate = self

        addCenterButtonWithImage()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        Tools.configNavBarGlobal()

        if let tabInnerVC = selectedViewController as? TabInnerControllerProtocol {
            navigationItem.titleView = tabInnerVC.customTitleView()
        }
    }
}


extension HomeTabbedVC: UITabBarControllerDelegate {
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        if let tabInnerVC = viewController as? TabInnerControllerProtocol {
            navigationItem.titleView = tabInnerVC.customTitleView()
        }
        if let _ = viewController as? EventsPagerVC {
            let buttonImage = UIImage(named: "map_icon")!
            let rightButton: UIBarButtonItem = UIBarButtonItem(image: buttonImage, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(HomeTabbedVC.actShowMap(_:)))
            navigationItem.rightBarButtonItem = rightButton

        }
        else {
            navigationItem.rightBarButtonItem = nil
        }
    }

    func actShowMap(sender: AnyObject) {
        if let vc = selectedViewController as? EventsPagerVC {
            vc.pushToMap()
        }
    }

}


//MARK: - CenterVCDatasource

extension HomeTabbedVC: CenterControllerProtocol {
    func setCenterDelegate(delegate: CenterViewControllerDelegate) {
        centerDelegate = delegate
    }
}


//MARK: - SidePanelDelegate
extension HomeTabbedVC: SidePanelDelegate {

    func actionWith(action action: Actions, closeAllPanels: Bool) {

        if closeAllPanels {
            centerDelegate?.collapseSidePanels()
        }

        switch action {
        case .Timeline, .GoodNews, .Redeem:
            if let point = action.webPoint {
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                appDelegate.doMenuGoToWeb(point.safariURL)
            }
        case .Settings:
            Tools.alert("INFO", message: "Comming soon").show()
        case .Logout:
            U4G_Core.logout { (dict, error) -> () in
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    let tracker = GAI.sharedInstance().defaultTracker
                    let params = [kGAIHitType : "appview", kGAIScreenName : "Logout"]
                    tracker.send(params)
                    NSNotificationCenter.defaultCenter().postNotification(NSNotification(name: Defines.kLoginNeededNotification, object: nil))
                }
            }

        default:
            centerDelegate?.setCenterVCForAction(action)
        }
    }
}

//MARK: - TabInnerControllerDelegate
extension HomeTabbedVC: TabInnerControllerDelegate {

}
