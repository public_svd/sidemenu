//
//  TabInnerVC.swift
//  U4G
//
//  Created by SvD on 18.11.15.
//  Copyright © 2015 SvD. All rights reserved.
//

import UIKit

class TabInnerVC: UIViewController {

    var activeFilter = 0 {
        didSet {
            updateViewForActiveFilter()
        }
    }
    
    var activeVC: UIViewController!
    var innerDelegate: TabInnerControllerDelegate?
    
    var filtersTitle: [String] {
        get {
            return filterTitles()
        }
    }
    
    var controllers: [String] {
        get {
            return controllersID()
        }
    }

    lazy var titleLabel: UILabel = {
        let resultLabel = UILabel(frame: CGRectZero)
        resultLabel.font = UIFont.init(name: Fonts.light, size: 17)
        resultLabel.textColor = UIColor.whiteColor()
        resultLabel.textAlignment = NSTextAlignment.Center
        resultLabel.text = self.filtersTitle[0]
        resultLabel.sizeToFit()
        return resultLabel
    }()

    lazy var titleView: UIView? = {
        let result = UIView(frame: CGRectZero)
        result.addSubview(self.titleLabel)
        self.configTitleView(result)
        return result
    }()


    func configTitleView(title: UIView?) {
        title?.frame = titleLabel.bounds
        if filtersTitle.count > 1 {
            
            let resultHeight: CGFloat = 44
            let buttonWidth: CGFloat = 30
            
            let leftButton = UIButton(type: .Custom)
            leftButton.frame = CGRectMake(0, 0, buttonWidth, resultHeight)
            leftButton.setImage(UIImage(named: "arrow_left_ic"), forState: .Normal)
            leftButton.addTarget(self, action: #selector(TabInnerVC.leftButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
            
            let rightButton = UIButton(type: .Custom)
            rightButton.frame = CGRectMake(buttonWidth + 140, 0, buttonWidth, resultHeight)
            rightButton.setImage(UIImage(named: "arrow_right_ic"), forState: .Normal)
            rightButton.addTarget(self, action: #selector(TabInnerVC.rightButtonTapped), forControlEvents: UIControlEvents.TouchUpInside)
            
            let containerView = UIView(frame: CGRectMake(buttonWidth, 0, 140, resultHeight))
            containerView.clipsToBounds = true

            titleLabel.frame = containerView.bounds
            titleLabel.userInteractionEnabled = true
            containerView.addSubview(titleLabel)

            title?.addSubview(containerView)
            
            title?.addSubview(leftButton)
            title?.addSubview(rightButton)
            title?.frame = CGRectMake(0, 0, 200, resultHeight)

            let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(TabInnerVC.handlePanGesture(_:)))
            titleLabel.addGestureRecognizer(panGestureRecognizer)

        }
    }

    func handlePanGesture(recognizer: UIPanGestureRecognizer) {
        let gestureIsDraggingFromLeftToRight = (recognizer.velocityInView(view).x > 0)

        switch(recognizer.state) {
        case .Began:
            #if DEBUG
            #endif
            print("Pan began at point: \(recognizer.locationInView(recognizer.view))")

        case .Changed:
            let recOffset = recognizer.translationInView(view).x
            let recView = recognizer.view!
            recView.transform = CGAffineTransformTranslate(recView.transform, recOffset, 0)
            recognizer.setTranslation(CGPointZero, inView: view)

        case .Ended:
            let thresholdOffset: CGFloat = 35
            let recView = recognizer.view!
            if abs(recView.transform.tx) > thresholdOffset {
                let width = recView.bounds.width
                if gestureIsDraggingFromLeftToRight {
                    UIView.animateWithDuration(0.3, animations: { () -> Void in
                        recView.transform.tx = width
                    })
                    leftButtonTapped()
                    recView.transform.tx = -width
                }
                else {
                    UIView.animateWithDuration(0.3, animations: { () -> Void in
                        recView.transform.tx = -width
                    })
                    rightButtonTapped()
                    recView.transform.tx = width
                }
            }
            UIView.animateWithDuration(0.3, animations: { () -> Void in
                recView.transform = CGAffineTransformIdentity
            })

        default:
            break
        }
    }

    func leftButtonTapped() {
        
        if activeFilter == 0 {
            activeFilter = filtersTitle.count - 1
        }
        else {
            activeFilter -= 1
        }
    }
    
    func rightButtonTapped() {

        if activeFilter == filtersTitle.count - 1 {
            activeFilter = 0
        }
        else {
            activeFilter += 1
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateViewForActiveFilter()
    }

    func updateViewForActiveFilter() {
        for vc in childViewControllers {
            vc.view.removeFromSuperview()
            vc.removeFromParentViewController()
        }

        let controllerId = controllers[activeFilter]
        let newVC = UIStoryboard.mainStoryboard().instantiateViewControllerWithIdentifier(controllerId)
        activeVC = newVC
        activeVC.view.frame = view.frame

        addChildViewController(activeVC)
        view.addSubview(activeVC.view)
        activeVC.didMoveToParentViewController(self)

        titleLabel.text = filtersTitle[activeFilter]
        filterDidUpdated()
    }

    func filterDidUpdated() {
        #if DEBUG
        #endif
        print("Filter Updated")
    }

}

extension TabInnerVC: TabInnerControllerProtocol {

    func setInnerDelegate(delegate: TabInnerControllerDelegate) {
        innerDelegate = delegate
    }

    func customTitleView() -> UIView? {
        return titleView
    }

    func navigationBarItems() -> [UINavigationItem]? {
        let result = [UINavigationItem]()        
        return result
    }
    
    func filterTitles() -> [String] {
        return ["One"]
    }
    
    func controllersID() -> [String] {
        return ["EventsListVC"]
    }
}