//
//  MenuVC.swift
//  U4G
//
//  Created by Sergey on 04.08.15.
//  Copyright (c) 2015 SvD. All rights reserved.
//

import UIKit

enum Actions: Int {

    case Timeline = 0, Feeds, GoodNews, Redeem, Count
    case About, Support, Legal, Settings, Count2
    case Logout
    case Profile
    
    var description: String {
        var result = ""
        switch self {
        case .Feeds:
            result = "Feed"
        case .Timeline:
            result = "Timeline"
        case .GoodNews:
            result = "Good News"
        case .Redeem:
            result = "Reedem"
        case .Settings:
            result = "Settings"
        case .Legal:
            result = "Legal"
        case .Support:
            result = "Help & Support"
        case .About:
            result = "About U4G"
        case .Count, .Count2, .Logout, .Profile:
            break
        }
        return result
    }
    
    var controllerID: String {
        var result = ""
        switch self {
        case .Legal, .Support:
            result = "TermsVC"
        case .About:
            result = "AboutVC"
        case .Count, .Count2, .Logout:
            break
        case .Profile:
            result = "ProfileVC2"
        default:
            break
        }
        return result
    }
    
    var webPoint: WebPoints? {
        var result: WebPoints?
        switch self {
        case .Feeds:
            result = .Feeds
        case .Timeline:
            result = .TimeLine
        case .GoodNews:
            result = .GoodNews
        case .Redeem:
            result = .Rewards
        case .Legal:
            result = .Terms
        case .Support:
            result = .Support
        default:
            break
        }
        return result
    }
}

class MenuVC: UIViewController {
    
    @IBOutlet weak var viewSigned: UIView!
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var labUserName: UILabel!
    @IBOutlet weak var labEmail: UILabel!
    @IBOutlet weak var labPoints: UILabel!
    
    @IBOutlet weak var viewTable: UITableView!
    @IBOutlet weak var viewTableAddition: UITableView!
    
    
    let user = Tools.sharedUser
    var sidePanelDelegate: SidePanelDelegate?
    
    @IBAction func actLogout(sender: AnyObject) {
        sidePanelDelegate?.actionWith(action: Actions.Logout, closeAllPanels: true)
    }
    
    func updateUserInfo() {
        labUserName.text = user.fullName
        labEmail.text = user.email
        if let points = user.kPoints {
            labPoints.text = "\(points)"
        }
        dispatch_async(dispatch_get_main_queue()) {[weak self] () -> Void in
            if let imgPath = self?.user.thumbnail, let imgData = NSData(contentsOfURL: NSURL(string: imgPath)!) {
                self?.imgPhoto.image = UIImage(data: imgData)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MenuVC.handleTapGesture(_:)))
        imgPhoto.addGestureRecognizer(tapGestureRecognizer)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        imgPhoto.layer.cornerRadius = CGRectGetHeight(imgPhoto.bounds) / 2.0
        viewSigned.backgroundColor = UIColor(patternImage: UIImage(named: "bg_menu_header")!)
        updateUserInfo()
    }
}


extension MenuVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = (tableView == viewTable) ? Actions.Count.rawValue : (Actions.Count2.rawValue - Actions.Count.rawValue - 1)
        
        return count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let rowHeight: CGFloat = (tableView == viewTable) ? 40.0 : 35.0
        return rowHeight
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellID = (tableView == viewTable) ? "MenuCell" : "MenuCell2"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellID) as? MenuCell
        let currentCell = cell!
        
        let actualRow: Int = (tableView == viewTable) ? indexPath.row : (Actions.Count.rawValue + 1 + indexPath.row)
        currentCell.labTitle.text = Actions(rawValue: actualRow)?.description
        
        return currentCell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        let actualRow: Int = (tableView == viewTable) ? indexPath.row : (Actions.Count.rawValue + 1 + indexPath.row)
        sidePanelDelegate?.actionWith(action: Actions(rawValue: actualRow)!, closeAllPanels: true)
    }
}


extension MenuVC: UIGestureRecognizerDelegate {
    func handleTapGesture(recognizer: UITapGestureRecognizer) {
        sidePanelDelegate?.actionWith(action: Actions.Profile, closeAllPanels: true)
    }
}


extension MenuVC: SidePanelProtocol {
    func setSidePanelDelegate(delegate: SidePanelDelegate) {
        sidePanelDelegate = delegate
    }
}



class MenuCell: UITableViewCell {
    
    @IBOutlet weak var labTitle: UILabel!
    
}

