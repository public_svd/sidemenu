//
//  SDPagerVC.swift
//  SDPagingViewController
//
//  Created by SvD on 17.12.15.
//  Copyright © 2015 SvD. All rights reserved.
//

import UIKit

class SDPagerVC: UIPageViewController {

    var pages: [UIViewController] {
        let story = UIStoryboard.mainStoryboard()
        return [story.instantiateViewControllerWithIdentifier("CommunitiesVC"),
                story.instantiateViewControllerWithIdentifier("VirtualEventsListVC"),
                story.instantiateViewControllerWithIdentifier("InfluencersVC")]
    }

    lazy var titleView: UIView = {
        let resultLabel = UILabel(frame: CGRectZero)
        resultLabel.font = UIFont.init(name: Fonts.light,  size: 17)
        resultLabel.textColor = UIColor.whiteColor()
        resultLabel.textAlignment = NSTextAlignment.Center
        resultLabel.text = self.filterTitles()[self.currentIndex]
        resultLabel.sizeToFit()
        
        return resultLabel
       
    }()
    
    var currentIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self

        let startingViewController = pages[0]
        let viewControllers = [startingViewController]
        setViewControllers(viewControllers, direction: .Forward, animated: false) { [weak self] done in
            if done {
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    self?.setViewControllers(viewControllers, direction: .Forward, animated: false) { done in }
                }
            }
        }

    }


    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if dataSource == nil {
            dataSource = self
        }
    }

}


// MARK: - Page View Controller Data Source
extension SDPagerVC: UIPageViewControllerDataSource {

    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {

        if (currentIndex == 0) {
            currentIndex = pages.count
        }
        if currentIndex > 0 {
            currentIndex -= 1
        }

        return pages[currentIndex]
    }

    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {

        currentIndex += 1
        if currentIndex == pages.count {
            currentIndex = 0
        }

        return pages[currentIndex]
    }

}


// MARK: - UIPageViewController delegate methods
extension SDPagerVC: UIPageViewControllerDelegate {

    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let vc = pageViewController.viewControllers?[0] {
            let labTitle = titleView as! UILabel
            labTitle.text = vc.title
            labTitle.sizeToFit()
        }
    }

}

extension SDPagerVC: TabInnerControllerProtocol {
    
    func setInnerDelegate(delegate: TabInnerControllerDelegate) {
    }
    
    func customTitleView() -> UIView? {
        return titleView
    }
    
    func navigationBarItems() -> [UINavigationItem]? {
        return nil
    }
    
    func filterTitles() -> [String] {
        return [pages[0].title!, pages[1].title!, pages[2].title!]
    }
    
    func controllersID() -> [String] {
        return []
    }
}

